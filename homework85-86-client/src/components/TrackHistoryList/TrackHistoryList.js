import React from 'react';
import {Panel} from "react-bootstrap";
import PropTypes from 'prop-types';


const TrackHistoryList = props => {


    return (
        <Panel>
            <Panel.Heading>{props.track}</Panel.Heading>
            <Panel.Body>{props.author} {props.album}</Panel.Body>
            <Panel.Footer>{props.datetime}</Panel.Footer>
        </Panel>
    );
};

TrackHistoryList.propTypes = {
    id: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    track: PropTypes.string.isRequired,
    album: PropTypes.string.isRequired,
    datetime: PropTypes.string

};

export default TrackHistoryList;