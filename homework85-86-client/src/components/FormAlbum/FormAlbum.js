import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import FormElement from "../UI/FormElement/FormElement";
import {fetchArtists} from "../../store/actions/artists";
import {connect} from "react-redux";

class FormAlbum extends Component {
    state = {
        name: '',
        year: '',
        author: '',
        image: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        const author = this.props.artists.map(artist => {
            return {
                id: artist._id,
                name: artist.name
            }
        });
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName='name'
                    title="Album name"
                    type='text'
                    value={this.state.name}
                    changeHandler={this.inputChangeHandler}
                    required
                />

                <FormElement
                    propertyName='year'
                    title="Album year"
                    type='number'
                    value={this.state.year}
                    changeHandler={this.inputChangeHandler}
                    required
                />

                <FormElement
                    propertyName="author"
                    title="Album author"
                    type='select'
                    options={author}
                    value={this.state.author}
                    changeHandler={this.inputChangeHandler}
                    required
                />
                <FormElement
                    propertyName="image"
                    title="Artist image"
                    type="file"
                    changeHandler={this.fileChangeHandler}
                />
                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => {
    return {
        artists: state.artists.artists
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchArtists: () => dispatch(fetchArtists())
    }
};

export default connect(mapStateToProps, mapDispatchToProps) (FormAlbum);
