import React, {Fragment} from "react";
import {MenuItem, Nav, NavDropdown, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const UserName = ({user, logout}) => {
    const navTitle = (
        <Fragment>
            hello, <b>{user.username}</b>!
        </Fragment>
    );
    return (
        <Nav pullRight>
            <NavDropdown title={navTitle} id="user-menu">
                <LinkContainer to="/track_history/" exact>
                    <NavItem>Track History</NavItem>
                </LinkContainer>
                <LinkContainer to="/artists/new" exact>
                    <NavItem>Add new artist</NavItem>
                </LinkContainer>
                <LinkContainer to="/albums/new" exact>
                    <NavItem>Add new album</NavItem>
                </LinkContainer>
                <LinkContainer to="/tracks/new" exact>
                    <NavItem>Add new track</NavItem>
                </LinkContainer>
                <MenuItem divider/>
                <MenuItem onClick={logout}>Logout</MenuItem>
            </NavDropdown>
        </Nav>
    )

};

export  default  UserName;