import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import AnonymousMenu from "./Menus/AnonymousMenu";
import UserMenu from "./Menus/UserMenu";

const Toolbar = ({user, logout}) => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Brand>
                <LinkContainer to="/" exact><a>Album</a></LinkContainer>
            </Navbar.Brand>
            <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav pullRight>
                <LinkContainer to="/" exact>
                    <NavItem>Artists</NavItem>
                </LinkContainer>

                {user ? <UserMenu user={user} logout={logout}/> : <AnonymousMenu /> }
            </Nav>
        </Navbar.Collapse>
    </Navbar>
);

export default Toolbar;