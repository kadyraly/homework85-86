import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import FormElement from "../UI/FormElement/FormElement";

class FormArtist extends Component {
    state = {
        name: '',
        information: '',
        image: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName='name'
                    title="Artist name"
                    type='text'
                    value={this.state.name}
                    changeHandler={this.inputChangeHandler}
                    required
                />

                <FormElement
                    propertyName="information"
                    title="Artist information"
                    type="textarea"
                    value={this.state.information}
                    changeHandler={this.inputChangeHandler}
                    required
                />
                <FormElement
                    propertyName="image"
                    title="Artist image"
                    type="file"
                    changeHandler={this.fileChangeHandler}
                />
                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default FormArtist;
