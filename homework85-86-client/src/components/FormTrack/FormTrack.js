import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import FormElement from "../UI/FormElement/FormElement";
import {fetchAlbums} from "../../store/actions/albums";
import {connect} from "react-redux";

class FormTrack extends Component {
    state = {
        name: '',
        duration: '',
        album: '',
        number: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.onSubmit(this.state);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    render() {
        const album = this.props.albums.map(album => {
            return {
                id: album._id,
                name: album.name
            }
        });

        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName='name'
                    title="Track name"
                    type='text'
                    value={this.state.name}
                    changeHandler={this.inputChangeHandler}
                    required
                />

                <FormElement
                    propertyName='duration'
                    title="Track duration"
                    type='text'
                    value={this.state.duration}
                    changeHandler={this.inputChangeHandler}
                    required
                />

                <FormElement
                    propertyName="album"
                    title="Track album"
                    type='select'
                    options={album}
                    value={this.state.album}
                    changeHandler={this.inputChangeHandler}
                    required
                />
                <FormElement
                    propertyName='number'
                    title="Track number"
                    type='number'
                    value={this.state.number}
                    changeHandler={this.inputChangeHandler}
                    required
                />
                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => {
    return {
        albums: state.albums.albums
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchAlbums: (id) => dispatch(fetchAlbums(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps) (FormTrack);