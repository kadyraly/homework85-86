import React, {Fragment} from 'react';
import {Button, Panel} from "react-bootstrap";
import PropTypes from 'prop-types';
const TrackList = props => {

    return (
        <Panel>
            <Panel.Body>
                {props.number} {props.name}
                <Button className="pull-right" onClick={props.click} bsStyle="primary" >Play</Button>

                {props.user && props.user.role === 'admin' ?
                    < Fragment >
                        {!props.published ? <Button className="pull-right" onClick={props.clickPub} bsStyle="primary" >Publish</Button> : null}
                        <Button className="pull-right" onClick={props.clickDel} bsStyle="primary" >Delete</Button>
                    </Fragment> : null
                }
            </Panel.Body>
        </Panel>
    );
};

TrackList.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    number: PropTypes.number.isRequired

};

export default TrackList;