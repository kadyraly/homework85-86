import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import {createArtist} from "../../store/actions/artists";
import FormArtist from "../../components/Form/FormArtist";


class NewArtist extends Component {
    createArtist = artistData => {
        this.props.onArtistCreated(artistData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <PageHeader>New artist</PageHeader>
                <FormArtist onSubmit={this.createArtist} />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onArtistCreated: artistData => {
            return dispatch(createArtist(artistData))
        }
    }
};

export default connect(null, mapDispatchToProps)(NewArtist);