import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import {Link} from "react-router-dom";
import {deleteArtist, fetchArtists, publishArtist} from "../../store/actions/artists";
import ArtistItem from "../../components/ArtistItem/ArtistItem";


class Artists extends Component {
    state = {
        deletedArtist: null
    };

    componentDidMount() {
        this.props.onFetchArtists();
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.deletedArtist !== this.state.deletedArtist) {
            this.props.onFetchArtists();
        }
    }

    onDeleteArtist = async (id) => {
        await this.props.deleteArtist(id);
        this.setState(() => {
            return {deletedArtist: id}
        });

    };

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Artists
                    <Link to="/artists/new">

                    </Link>
                </PageHeader>

                {this.props.artists.map(artist => (
                    <ArtistItem
                        key={artist._id}
                        id={artist._id}
                        name={artist.name}
                        image={artist.image}
                        published={artist.published}
                        user={this.props.user}
                        click={this.props.user ? () => this.onDeleteArtist(artist._id) : null}
                        clickPub={this.props.user ? () => this.props.publishArtist(artist._id) : null}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        artists: state.artists.artists,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchArtists: () => dispatch(fetchArtists()),
        publishArtist: (id) => dispatch(publishArtist(id)),
        deleteArtist: (id) => dispatch(deleteArtist(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Artists);