import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';

import { PageHeader} from "react-bootstrap";

import TrackHistoryList from "../../components/TrackHistoryList/TrackHistoryList";
import {fetchHistory} from "../../store/actions/trackHistory";

class TrackHistory extends Component {
    componentDidMount() {
        if(this.props.user) {
            this.props.onFetchHistory(this.props.user.token);
        } else {
            this.props.history.push('/login');
        }
    }

    render() {
        if(!this.props.histories) {
            return <div>Loading</div>
        }
        return (
            <Fragment>
                <PageHeader>
                    TrackHistory
                </PageHeader>

                {this.props.histories.map(history => {
                    console.log(history.track.album.author)
                    if(history.track.album.author) {
                        return (
                            <TrackHistoryList
                                key={history._id}
                                id={history._id}
                                author={history.track.album.author.name}
                                track={history.track.name}
                                album={history.track.album.name}
                                datetime={history.datetime}
                            />
                        )
                    }

                })}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        histories: state.histories.histories,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchHistory: (id) => dispatch(fetchHistory(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);