import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';

import AlbumList from "../../components/AlbumList/AlbumList";
import {deleteAlbum, fetchAlbums, publishAlbum} from "../../store/actions/albums";
import { PageHeader} from "react-bootstrap";


class Album extends Component {
    state = {
        deletedAlbum: null
    };

    componentDidMount() {
        this.props.onFetchAlbums(this.props.match.params.id);
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.deletedAlbum !== this.state.deletedAlbum) {
            this.props.onFetchAlbums(this.props.match.params.id);
        }
    }

    onDeleteAlbum = async (id) => {
        await this.props.deleteAlbum(id);
        this.setState(() => {
            return {deletedAlbum: id}
        });

    };

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Albums
                </PageHeader>

                {this.props.albums.map(album => (
                    <AlbumList
                        key={album._id}
                        id={album._id}
                        year={album.year}
                        image={album.image}
                        published={album.published}
                        user={this.props.user}
                        click={this.props.user ? () => this.onDeleteAlbum(album._id) : null}
                        clickPub={this.props.user ? () => this.props.publishAlbum(album._id) : null}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        albums: state.albums.albums,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchAlbums: (id) => dispatch(fetchAlbums(id)),
        publishAlbum: (id) => dispatch(publishAlbum(id)),
        deleteAlbum: (id) => dispatch(deleteAlbum(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Album);