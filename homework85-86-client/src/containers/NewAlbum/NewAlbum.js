import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import FormAlbum from "../../components/FormAlbum/FormAlbum";
import {createAlbum} from "../../store/actions/albums";
import {fetchArtists} from "../../store/actions/artists";



class NewAlbum extends Component {
    componentDidMount() {
        this.props.onFetchArtists();
    }
    createAlbum = albumData => {
        this.props.onAlbumCreated(albumData).then(() => {
        });
    };

    render() {
        return (
            <Fragment>
                <PageHeader>New album</PageHeader>
                <FormAlbum onSubmit={this.createAlbum} />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAlbumCreated: albumData => {
            return dispatch(createAlbum(albumData))
        },
        onFetchArtists: () => dispatch(fetchArtists())
    }
};

export default connect(null, mapDispatchToProps)(NewAlbum);