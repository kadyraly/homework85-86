import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import FormTrack from "../../components/FormTrack/FormTrack";
import {createTrack} from "../../store/actions/tracks";
import {fetchAlbums} from "../../store/actions/albums";


class NewTrack extends Component {
    componentDidMount() {
        this.props.onFetchAlbums();
    }

    createTrack = trackData => {
        this.props.onTrackCreated(trackData).then(() => {
        });
    };

    render() {
        return (
            <Fragment>
                <PageHeader>New track</PageHeader>
                <FormTrack onSubmit={this.createTrack} />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onTrackCreated: trackData => {
            return dispatch(createTrack(trackData))

        },
        onFetchAlbums: () => dispatch(fetchAlbums())
    }
};

export default connect(null, mapDispatchToProps)(NewTrack);