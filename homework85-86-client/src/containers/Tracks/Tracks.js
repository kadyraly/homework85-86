import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';

import { PageHeader} from "react-bootstrap";
import {deleteTrack, fetchTracks, publishTrack} from "../../store/actions/tracks";
import TrackList from "../../components/TrackList/TrackList";
import {createHistory} from "../../store/actions/trackHistory";

class Tracks extends Component {
    state = {
        deletedTrack: null
    };

    componentDidMount() {
        this.props.onFetchTracks(this.props.match.params.id);
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.deletedTrack !== this.state.deletedTrack) {
            this.props.onFetchTracks(this.props.match.params.id);
        }
    }

    onDeleteTrack = async (id) => {
        await this.props.deleteTrack(id);
        this.setState(() => {
            return {deletedTrack: id}
        });

    };

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Tracks
                </PageHeader>

                {this.props.tracks.map(track => (
                    <TrackList
                        key={track._id}
                        id={track._id}
                        name={track.name}
                        number={track.number}
                        published={track.published}
                        user={this.props.user}
                        click={this.props.user ? () => this.props.createHistory({track: track._id}, this.props.user.token) : null}
                        clickDel={this.props.user ? () =>  this.onDeleteTrack(track._id): null}
                        clickPub={this.props.user ? () => this.props.publishTrack(track._id) : null}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        tracks: state.tracks.tracks,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchTracks: (id) => dispatch(fetchTracks(id)),
        createHistory: (historyData, token) => dispatch(createHistory(historyData, token)),
        publishTrack: (id) => dispatch(publishTrack(id)),
        deleteTrack: (id) => dispatch(deleteTrack(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);