import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./containers/Layout/Layout";
import Artists from "./containers/Artists/Artists";
import Album from "./containers/Album/Album";
import Tracks from "./containers/Tracks/Tracks";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import NewArtist from "./containers/NewArtist/NewArtist";
import NewAlbum from "./containers/NewAlbum/NewAlbum";
import NewTrack from "./containers/NewTrack/NewTrack";


class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={Artists} />
                    <Route path="/artists/new" exact component={NewArtist} />
                    <Route path="/albums" exact component={Album} />
                    <Route path="/artists/:id" exact component={Album} />
                    <Route path="/albums/new" exact component={NewAlbum} />
                    <Route path="/tracks" exact component={Tracks} />
                    <Route path="/albums/:id" exact component={Tracks} />
                    <Route path="/tracks/new" exact component={NewTrack} />
                    <Route path="/track_history" exact component={TrackHistory} />
                    <Route path="/register" exact component={Register} />
                    <Route path="/login" exact component={Login} />
                </Switch>
            </Layout>
        )
    }
}

export default App;
