import {FETCH_TRACKS_SUCCESS, PUBLISH_TRACK_SUCCESS} from "../actions/actionTypes";


const initialState = {
    tracks: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_TRACKS_SUCCESS:
            return {...state, tracks: action.tracks};
        case PUBLISH_TRACK_SUCCESS:
            let tracks = [...state.tracks];
            const index = tracks.findIndex(track => action.publishedTrack._id === track._id);

            let currentTrack = {...tracks[index]};
            currentTrack.published = true;
            tracks[index] = currentTrack;

            return {...state, tracks};
        default:
            return state;
    }
};

export default reducer;