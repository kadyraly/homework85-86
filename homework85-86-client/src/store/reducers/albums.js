import {FETCH_ALBUMS_SUCCESS, PUBLISH_ALBUM_SUCCESS} from "../actions/actionTypes";


const initialState = {
    albums: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_ALBUMS_SUCCESS:
            return {...state, albums: action.albums};
        case PUBLISH_ALBUM_SUCCESS:
            let albums = [...state.albums];
            const index = albums.findIndex(album => action.publishedAlbum._id === album._id);

            let currentAlbum = {...albums[index]};
            currentAlbum.published = true;
            albums[index] = currentAlbum;

            return {...state, albums};
        default:
            return state;
    }
};

export default reducer;