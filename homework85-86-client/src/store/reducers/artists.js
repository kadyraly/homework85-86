import {FETCH_ARTISTS_SUCCESS, PUBLISH_ARTIST_SUCCESS} from "../actions/actionTypes";


const initialState = {
    artists: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_ARTISTS_SUCCESS:
            return {...state, artists: action.artists};
        case PUBLISH_ARTIST_SUCCESS:
            let artists = [...state.artists];
            const index = artists.findIndex(artist => action.publishedArtist._id === artist._id);

            let currentArtist = {...artists[index]};
            currentArtist.published = true;
            artists[index] = currentArtist;

            return {...state, artists};
        default:
            return state;
    }
};

export default reducer;