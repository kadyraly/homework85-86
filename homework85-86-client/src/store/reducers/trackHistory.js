import {FETCH_HISTORY_SUCCESS} from "../actions/actionTypes";


const initialState = {
    histories: null
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_HISTORY_SUCCESS:
            return {...state, histories: action.histories};
        default:
            return state;
    }
};

export default reducer;