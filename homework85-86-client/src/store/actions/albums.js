import axios from '../../axios-api';
import {
    CREATE_ALBUM_SUCCESS, DELETE_ALBUM_SUCCESS, FETCH_ALBUMS_SUCCESS, PUBLISH_ALBUM_SUCCESS,

} from "./actionTypes";
import {push} from "react-router-redux";


export const fetchAlbumsSuccess = albums => {
    return {type: FETCH_ALBUMS_SUCCESS, albums};
};

export const fetchAlbums = (id) => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        if(id) {
            axios.get(user && user.role === 'admin' ? '/albums/admin?artists=' + id : '/albums?artists=' + id).then(
                response => dispatch(fetchAlbumsSuccess(response.data))
            );
        } else {
            axios.get(user && user.role === 'admin' ? '/albums/admin' :'/albums').then(
                response => dispatch(fetchAlbumsSuccess(response.data))
            );
        }
    }
};

export const createAlbumSuccess = () => {
    return {type: CREATE_ALBUM_SUCCESS};
};

export const createAlbum = albumData => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.post('/albums', albumData, {headers}).then(
            response => {
                dispatch(push('/artists/' + response.data.author));
                dispatch(createAlbumSuccess())
            }
        );
    };
};

export const publishAlbumSuccess = (publishedAlbum) => {
    return {type: PUBLISH_ALBUM_SUCCESS, publishedAlbum};
};

export const publishAlbum = (id) => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.put('/albums/publish/' + id, {headers}).then(
            response => dispatch(publishAlbumSuccess(response.data))
        );
    };
};
export const deleteAlbumSuccess = () => {
    return {type: DELETE_ALBUM_SUCCESS};
};

export const deleteAlbum = (id) => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.delete('/albums/publish/' + id, {headers}).then(
            response => dispatch(deleteAlbumSuccess())
        );
    };
};

