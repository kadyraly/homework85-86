import axios from '../../axios-api';
import {
    CREATE_ARTIST_SUCCESS, DELETE_ARTIST_SUCCESS, FETCH_ARTISTS_SUCCESS,
    PUBLISH_ARTIST_SUCCESS
} from "./actionTypes";


export const fetchArtistsSuccess = artists => {
    return {type: FETCH_ARTISTS_SUCCESS, artists};
};

export const fetchArtists = () => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        axios.get(user && user.role === 'admin' ? '/artists/admin' : '/artists').then(
            response => dispatch(fetchArtistsSuccess(response.data))
        );
    }
};

export const createArtistSuccess = () => {
    return {type: CREATE_ARTIST_SUCCESS};
};

export const createArtist = artistData => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.post('/artists', artistData, {headers}).then(
            response => dispatch(createArtistSuccess())
        );
    };
};

export const publishArtistSuccess = (publishedArtist) => {
    return {type: PUBLISH_ARTIST_SUCCESS, publishedArtist};
};

export const publishArtist = (id) => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.put('/artists/publish/' + id, {headers}).then(
            response => dispatch(publishArtistSuccess(response.data))
        );
    };
};
export const deleteArtistSuccess = () => {
    return {type: DELETE_ARTIST_SUCCESS};
};

export const deleteArtist = (id) => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.delete('/artists/publish/' + id, {headers}).then(
            response => dispatch(deleteArtistSuccess())
        );
    };
};



