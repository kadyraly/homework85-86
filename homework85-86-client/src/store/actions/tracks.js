import axios from '../../axios-api';
import {
    CREATE_TRACK_SUCCESS, DELETE_TRACK_SUCCESS, FETCH_TRACKS_SUCCESS,
    PUBLISH_TRACK_SUCCESS
} from "./actionTypes";
import {push} from "react-router-redux";


export const fetchTracksSuccess = tracks => {
    return {type: FETCH_TRACKS_SUCCESS, tracks};
};

export const fetchTracks = (id) => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        axios.get(user && user.role === 'admin' ? '/tracks/admin' : '/tracks?album=' + id).then(
            response => dispatch(fetchTracksSuccess(response.data))
        );
    }
};

export const createTrackSuccess = () => {
    return {type: CREATE_TRACK_SUCCESS};
};

export const createTrack = trackData => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.post('/tracks', trackData, {headers}).then(
            response => {
                dispatch(push('/albums/' + response.data.album));
                dispatch(createTrackSuccess())
            }
        );
    };
};

export const publishTrackSuccess = (publishedTrack) => {
    return {type: PUBLISH_TRACK_SUCCESS, publishedTrack};
};

export const publishTrack= (id) => {
    return (dispatch, getState) => {
        console.log(id)
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.put('/tracks/publish/' + id, {headers}).then(
            response => dispatch(publishTrackSuccess(response.data))
        );
    };
};
export const deleteTrackSuccess = () => {
    return {type: DELETE_TRACK_SUCCESS};
};

export const deleteTrack = (id) => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.delete('/tracks/publish/' + id, {headers}).then(
            response => dispatch(deleteTrackSuccess())
        );
    };
};
