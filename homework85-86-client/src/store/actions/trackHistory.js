import axios from '../../axios-api';
import {CREATE_HISTORY_SUCCESS, FETCH_HISTORY_SUCCESS} from "./actionTypes";



export const createHistorySuccess = histories => {
    return {type: CREATE_HISTORY_SUCCESS, histories};
};
export const fetchHistorySuccess = histories => {
    return {type: FETCH_HISTORY_SUCCESS, histories};
};

export const fetchHistory = (token) => {
    return dispatch => {
        axios.get('/track_history', {headers: {"Token": token}}).then(
            response => {
                dispatch(fetchHistorySuccess(response.data))
            }
        );
    }
};

export const createHistory = (historyData, token) => {
    return dispatch => {
        axios.post('/track_history', historyData, {headers: {"Token": token}}).then(
            response => dispatch(createHistorySuccess(response.data))

        );
    }

};

