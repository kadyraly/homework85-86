import {createStore, applyMiddleware, compose, combineReducers} from 'redux';

import thunkMiddleware from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import {routerMiddleware, routerReducer} from 'react-router-redux';



import artistsReducer from './reducers/artists';
import albumsReducer from './reducers/albums';
import tracksReducer from './reducers/tracks';
import historyReducer from './reducers/trackHistory';
import usersReducer from './reducers/users';
import  {saveState, loadState} from "./localStorage";

const rootReducer = combineReducers({
    histories: historyReducer,
    tracks: tracksReducer,
    albums: albumsReducer,
    artists: artistsReducer,
    users: usersReducer,
    routing: routerReducer
});

export const history = createHistory();

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadState();
const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    console.log('subscribe');
    saveState({
        users: store.getState().users
    });
});

export default store;
