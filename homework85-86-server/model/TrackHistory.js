const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const TrackHistorySchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        required: true
    },
    track: {
        type: Schema.Types.ObjectId,
        ref: 'Tracks',
        required: true
    },
    datetime: {
        type: String
    }

});

const TrackHistory = mongoose.model('TrackHistory', TrackHistorySchema);
module.exports = TrackHistory;