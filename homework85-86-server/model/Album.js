const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const AlbumSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    year: {
        type: Number,
        required: true
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'Artists',
        required: true
    },
    published: {
        type: Boolean,
        default: false,
        required: true
    },
    image: String
});

const Album = mongoose.model('Albums', AlbumSchema);
module.exports = Album;