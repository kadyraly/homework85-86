const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const TrackSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    duration: {
        type: String,
        required: true
    },
    album: {
        type: Schema.Types.ObjectId,
        ref: 'Albums',
        required: true
    },
    published: {
        type: Boolean,
        default: false,
        required: true
    },
    number: {
        type: Number,
        required: true
    }

});

const Track = mongoose.model('Tracks', TrackSchema);
module.exports = Track;