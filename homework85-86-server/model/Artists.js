const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ArtistsSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    information: {
        type: String,
        required: true
    },
    published: {
      type: Boolean,
      default: false,
      required: true
    },
    image: String
});

const Artists = mongoose.model('Artists', ArtistsSchema);
module.exports = Artists;