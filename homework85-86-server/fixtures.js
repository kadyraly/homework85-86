const mongoose = require('mongoose');
const config = require('./config');

const Artists = require('./model/Artists');
const Album = require('./model/Album');
const User =require('./model/User');
const Tracks = require('./model/Tracks');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

const collection = ['artists', 'album', 'tracks', 'users'];

db.once('open', async () => {

    collection.forEach(async() => {
        try {
            await db.dropCollection('artists');
            await db.dropCollection('albums');
            await db.dropCollection('tracks');
            await db.dropCollection('users');

        } catch (e) {
            console.log('Collections were not present, skipping drop...');
        }
    });


    const [selena, lala] = await Artists.create({
        name: 'Selena',
        information: 'very good',
        image: 'selena-gomez.jpg'
    }, {
        name: 'Lala',
        information: 'very bad',
        image: 'singer.jpg'
    });

    const [selenaAlbum, lalaAlbum] = await Album.create({
        name: 'About Selena',
        year: 2017,
        author: selena._id,
        image: 'album-selena.jpg'
    }, {
        name: 'Lala',
        year: 2017,
        author: lala._id,
        image: 'singer.jpg'
    });

    await Tracks.create({
        name: 'Sing of Selena',
        duration: 3.45,
        album: selenaAlbum._id,
        author: selenaAlbum._id,
        number: 1
    }, {
        name: 'for you',
        duration: 3.30,
        album: selenaAlbum._id,
        author: lalaAlbum._id,
        number: 2
    }, {
        name: 'sorry',
        duration: 3.50,
        album: selenaAlbum._id,
        author: lalaAlbum._id,
        number: 3
    }, {
        name: 'please',
        duration: 3.10,
        album: selenaAlbum._id,
        author: selenaAlbum._id,
        number: 4
    }, {
        name: 'husband',
        duration: 2.50,
        album: selenaAlbum._id,
        author: lalaAlbum._id,
        number: 5
    }, {
        name: 'beach',
        duration: 3.00,
        album: selenaAlbum._id,
        author: selenaAlbum._id,
        number: 6
    },
    );

    await User.create({
        username: 'user',
        password: '123',
        role: 'user'
    }, {
        username: 'admin',
        password: 'admin123',
        role: 'admin'
    });

    db.close();
});