const express = require('express');
const TrackHistory = require('../model/TrackHistory');
const User = require('../model/User');
const Tracks = require('../model/Tracks');
const router = express.Router();


const createRouter = () => {
    router.get('/', async (req, res) => {
        const token = req.get('Token');
        const user = await User.findOne({token: token});
        TrackHistory.find({user: user._id}).populate({path: 'track', populate: {path: 'album', select: 'name', populate: {path: 'author', select: 'name'}}})
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
        });

    router.post('/', async (req, res) => {
        const tokenId = req.get('Token');
        const user = await User.findOne({token: tokenId}).select('_id');
        console.log(user);
        let historyData = req.body;
        if (user) {
            const track = req.body.track;
            const isTrack = await Tracks.find({_id: track}).select('_id');
            if (isTrack) {
                historyData.user = user._id;
                historyData.datetime = new Date();
                const history = new TrackHistory(historyData);
                await history.save();
                res.send({history});
            } else {
                res.sendStatus(400);
            }
        } else {
            res.sendStatus(401)
        }

    });

    return router;
};

module.exports = createRouter;