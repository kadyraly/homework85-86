const express = require('express');
const auth = require('../middleware/auth');


const path = require('path');
const Track = require('../model/Tracks');
const nanoid = require('nanoid');
const  config = require('../config');
const router = express.Router();

const ObjectId = require('mongodb').ObjectId;

const createRouter = () => {
    // Product index
    router.get('/', (req, res) => {
        if(req.query.album) {
            Track.find({album: req.query.album, published: true})
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        } else {
            Track.find({published: true})
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }
    });

    router.get('/admin', (req, res) => {
        if (req.query.album) {
            Track.find({author: req.query.album})
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        } else {
            Track.find()
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }
    });


    router.post('/', auth, (req, res) => {
        const trackData = req.body;
        const track = new Track(trackData);

        track.save().then(track => {
            res.send(track)
        })
    });


    router.put('/publish/:id', async (req, res) => {
        console.log(req.params.id)
        let track = await Track.findOne({_id: req.params.id});
        console.log(track)
        track.published = true;

        await track.save();

        res.send(track);
    });

    router.delete('/publish/:id', async (req, res) => {

        let track = await Track.findByIdAndRemove({_id: req.params.id});

        res.send(track);
    });


    return router;
};

module.exports = createRouter;