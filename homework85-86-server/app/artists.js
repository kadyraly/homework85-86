const express = require('express');

const multer = require('multer');
const path = require('path');
const Artists = require('../model/Artists');
const nanoid = require('nanoid');
const  config = require('../config');
const permit = require('../middleware/permit');
const auth = require('../middleware/auth');


const router = express.Router();

const ObjectId = require('mongodb').ObjectId;
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {

        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const createRouter = () => {
    // Product index
    router.get('/', (req, res) => {
        Artists.find({published: true})
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.get('/admin', (req, res) => {
        Artists.find()
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    // Product create
    router.post('/', [auth, upload.single('image')], (req, res) => {

        const artistData = req.body;

        if (req.file) {
            artistData.image = req.file.filename;
        } else {
            artistData.image = null;
        }

        const artist = new Artists(artistData);
        artist.save()
            .then(result => res.send(result))
            .catch(() => res.sendStatus(500));
    });

    router.put('/publish/:id', async (req, res) => {

        let artist = await Artists.findOne({_id: req.params.id});

        artist.published = true;

        await artist.save();

        res.send(artist);
    });

    router.delete('/publish/:id', async (req, res) => {

        let artist = await Artists.findByIdAndRemove({_id: req.params.id});

        res.send(artist);
    });


    return router;
};

module.exports = createRouter;